import time

import telebot

from notion_api import add_record, is_today_record

BOT_TOKEN = '6304180258:AAGocn9dTeJLeNTV1XZabLgvXr3fKMHh99U'
bot = telebot.TeleBot(BOT_TOKEN)

# Define a dictionary to keep track of the number of times the question is asked for each user
user_messages = {}
user_creds = {}
is_record = is_today_record()  # todo rerun every day


# Define the handler for the /start command
@bot.message_handler(commands=['start'])
def start(update):
    user_messages[update.chat.id] = []
    ask_integration(update)
    # ask_question(update.chat.id)
    # admin_reply(update)


# Define the handler for the text messages
@bot.message_handler(func=lambda message: True)
def handle_message(message):
    if message.chat.id not in user_creds:
        ask_notion_api_key(message)

    if is_record:
        bot.send_message(message.chat.id, "You already have a dairy record for today. Please come back tomorrow!")
    else:
        get_gratitudes_list(message)


def get_gratitudes_list(message):
    print(user_messages)
    if message.text.lower() == 'stop' or len(user_messages[message.chat.id]) == 5:
        bot.send_message(message.chat.id, "Okay, that's enough for today!")
        add_record(user_messages[message.chat.id])
    else:
        user_messages[message.chat.id].append(message.text)
        if len(user_messages[message.chat.id]) < 5:
            ask_question(message.chat.id)
        else:
            print(user_messages[message.chat.id])
            bot.send_message(message.chat.id, "That's it for today!")
            add_record(user_messages[message.chat.id])

#
# @bot.message_handler(chat_id=[123456789], commands=['start'])  # chat_id checks id corresponds to your list or not.
# def admin_reply(message):
#     while True:
#         bot.send_message(message.chat.id, "Allowed to receive an auto-message every 30 seconds.", time.sleep(30))


# Function to ask the question
def ask_question(chat_id):
    bot.send_message(chat_id, "Please write one more thing")


def ask_integration(message):
    if message.chat.id not in user_creds:
        bot.send_message(message.chat.id,
                         "Firstly, I need your Notion API key. Please check this link https://developers.notion.com/docs/create-a-notion-integration#:~:text=Getting%20started-,Create%20your%20integration%20in%20Notion,-The%20first%20step for information how to get it. Please paste it as a reply for this message")


def ask_notion_api_key(message):
    secret_parts = message.text.split('_')
    if len(message.text) == 50 and len(secret_parts[1]) == 43 and secret_parts[0] == 'secret':
        user_creds[message.chat.id] = {}
        user_creds[message.chat.id]['notion_key'] = message.text
    else:
        bot.send_message(message.chat.id,
                         "Unfortunately, this key doesn't look like Notion API key")


if __name__ == '__main__':
    # Start the bot's polling loop
    bot.polling()
