from datetime import date


def get_formatted_date():
    today = date.today()
    day = today.strftime("%d")  # Get the day of the month as a string

    formatted_date = today.strftime(f"{day} %B")

    if formatted_date.startswith('0'):
        formatted_date = formatted_date[1:]

    today_weekday = today.strftime('%A')

    return formatted_date + f', {today_weekday}'
