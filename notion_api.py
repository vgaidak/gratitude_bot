import json
from typing import List

import requests

from utils import get_formatted_date

TEST_PAGE_ID = 'f4ee1a25-276b-44c3-a4ea-11f6d0986838'
url = f"https://api.notion.com/v1/blocks/{TEST_PAGE_ID}/children"
NOTION_API_KEY = 'secret_HhTbb3k0ZE8Od3COhALsvdPlgZS2oXfLMmMXrnhiOHW'

headers = {
    'Authorization': f'Bearer {NOTION_API_KEY}',
    'Content-Type': 'application/json',
    "Notion-Version": "2022-06-28"
}


def add_record(text_points: List[str]):
    formatted_date = get_formatted_date()
    if not is_today_record():
        add_day_header(formatted_date)
        for point in text_points:
            add_bullet_point(point)


def add_day_header(today_date):
    data = {"children": [
        {
            "heading_3": {
                "rich_text": [
                    {
                        "text": {
                            "content": today_date
                        }
                    }
                ]
            }
        }
    ],
    }

    response = requests.patch(url, headers=headers, data=json.dumps(data))
    print(response.text)


def add_bullet_point(text):
    data = {"children": [
        {
            "bulleted_list_item": {
                "rich_text": [
                    {
                        "text": {
                            "content": f"{text}",
                        }
                    }
                ]
            }
        },
    ],
    }

    response = requests.patch(url, headers=headers, data=json.dumps(data))
    print(response.text)


def is_today_record():
    today_date = get_formatted_date()
    content = get_page()
    return True if today_date in content else False


def get_page():
    response = requests.get(url, headers=headers)
    return response.text